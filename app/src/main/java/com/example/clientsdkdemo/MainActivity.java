package com.example.clientsdkdemo;

import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.camera2.CameraDevice;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alicecallsbob.fcsdk.android.phone.Call;
import com.alicecallsbob.fcsdk.android.phone.CallCreationWithErrorException;
import com.alicecallsbob.fcsdk.android.phone.CallListener;
import com.alicecallsbob.fcsdk.android.phone.CallStatus;
import com.alicecallsbob.fcsdk.android.phone.CallStatusInfo;
import com.alicecallsbob.fcsdk.android.phone.MediaDirection;
import com.alicecallsbob.fcsdk.android.phone.Phone;
import com.alicecallsbob.fcsdk.android.phone.PhoneListener;
import com.alicecallsbob.fcsdk.android.phone.PhoneVideoCaptureResolution;
import com.alicecallsbob.fcsdk.android.phone.PhoneVideoCaptureSetting;
import com.alicecallsbob.fcsdk.android.phone.VideoSurface;
import com.alicecallsbob.fcsdk.android.phone.VideoSurfaceListener;
import com.alicecallsbob.fcsdk.android.uc.UC;
import com.alicecallsbob.fcsdk.android.uc.UCFactory;
import com.alicecallsbob.fcsdk.android.uc.UCListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements CallListener, VideoSurfaceListener, PhoneListener {

    private static UC uc;

    // create and cache references to the videoSurfaces needed for video calling
    VideoSurface localSurface;
    VideoSurface remoteSurface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button dial = this.findViewById(R.id.dial);
        dial.setEnabled(false);
        TextView destination = this.findViewById(R.id.destination);

        // bind to the Web Gateway by provisioning a session, and starting a WS connection
        this.connectToGateway();

        // wire up the UI handlers
        final CallListener callListener = this;
        dial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get Phone - and create a new outbound call!
                Phone phone = MainActivity.uc.getPhone();
                EditText editText = findViewById(R.id.destination);
                String destination = editText.getText().toString();
                log("Attempting to dial: " + destination);
                try {
                    Call call = phone.createCall(destination, MediaDirection.SEND_AND_RECEIVE, MediaDirection.SEND_AND_RECEIVE, callListener);

                    // if we want video in the call, we need to specify where the remote party video stream should go
                    // in our user interface!
                    call.setVideoView(remoteSurface);

                } catch (CallCreationWithErrorException e) {
                    log("Got an error trying to create a new call: " + e.getMessage());
                }
            }
        });

        findViewById(R.id.hangup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Phone phone = MainActivity.uc.getPhone();

                // ensure every call is ended!
                for(Call call : phone.getCurrentCalls())
                    call.end();
            }
        });
    }

    private void initVideoSurfaces()
    {
        final VideoSurfaceListener videoSurfaceListener = this;
        final PhoneListener phoneListener = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Phone phone = MainActivity.uc.getPhone();

                remoteSurface = phone.createVideoSurface(getApplicationContext(), new Point(640, 480), videoSurfaceListener);
                localSurface = phone.createVideoSurface(getApplicationContext(), new Point(160, 120), videoSurfaceListener);

                LinearLayout remoteView = findViewById(R.id.remote);
                LinearLayout localView = findViewById(R.id.local);

                // ensure the target containers for the video views are empty
                remoteView.removeAllViews();
                localView.removeAllViews();

                remoteView.addView(remoteSurface);
                localView.addView(localSurface);
                remoteView.setVisibility(View.VISIBLE);
                localView.setVisibility(View.VISIBLE);

                phone.setPreviewView(localSurface);

                phone.setCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
                phone.setPreferredCaptureResolution(PhoneVideoCaptureResolution.RESOLUTION_640x480);

                phone.setPreferredCaptureFrameRate(25);
                phone.addListener(phoneListener);
            }
        });

    }

    private void log(final String message)
    {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EditText log = findViewById(R.id.log);
                log.setText(message + "\n" + log.getText());
            }
        });
    }

    private void connectToGateway()
    {
        this.requestSessionProvisioned();
    }

    private void requestSessionProvisioned()
    {
        final MainActivity mainActivity = this;
        new Thread(new Runnable() {

            public void run() {
                try {

                    String urlOfEnterpriseSessionApi = "http://192.168.1.5:1234/init.php?user=alice";
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(urlOfEnterpriseSessionApi).openConnection();
                    InputStreamReader is = new InputStreamReader(httpURLConnection.getInputStream());
                    BufferedReader reader = new BufferedReader(is);
                    String sessionId = reader.readLine();
                    mainActivity.log("Got a session ID: " + sessionId);

                    initWebsocketWithGateway(sessionId);
                    initVideoSurfaces();
                } catch (Exception e) {
                    mainActivity.log("Got an error: " + e.getMessage());
                }
            }
        }).start();

    }

    private void initWebsocketWithGateway(String sessionId)
    {
        MainActivity.uc = UCFactory.createUc(getApplicationContext(), sessionId, new UCListener() {
            @Override
            public void onSessionStarted() {
                log("onSessionStarted");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.dial).setEnabled(true);
                    }
                });
            }

            @Override
            public void onSessionNotStarted() {
                log("onSessionNotStarted");
            }

            @Override
            public void onSystemFailure() {
                log("onSystemFailure");
            }

            @Override
            public void onConnectionLost() {
                log("onConnectionLost");
            }

            @Override
            public void onConnectionRetry(int i, long l) {
                log("onConnectionRetry");
            }

            @Override
            public void onConnectionReestablished() {
                log("onConnectionReestablished");
            }

            @Override
            public void onGenericError(String s, String s1) {
                log("onGenericError");
            }
        });

        MainActivity.uc.setNetworkReachable(true);
        MainActivity.uc.startSession();
    }

    @Override
    public void onDialFailed(Call call, String s, CallStatus callStatus) {
        this.log("onDialFailed");
    }

    @Override
    public void onCallFailed(Call call, String s, CallStatus callStatus) {
        this.log("onCallFailed");
    }

    @Override
    public void onMediaChangeRequested(Call call, boolean b, boolean b1) {
        this.log("onMediaChangeRequested");
    }

    @Override
    public void onStatusChanged(Call call, CallStatus callStatus) {
        this.log("onStatusChanged");
    }

    @Override
    public void onStatusChanged(Call call, CallStatusInfo callStatusInfo) {
        this.log("onStatusChanged");
    }

    @Override
    public void onRemoteDisplayNameChanged(Call call, String s) {
        this.log("onRemoteDisplayNameChanged");
    }

    @Override
    public void onRemoteMediaStream(Call call) {
        this.log("onRemoteMediaStream");
    }

    @Override
    public void onInboundQualityChanged(Call call, int i) {
        this.log("onInboundQualityChanged");
    }

    @Override
    public void onRemoteHeld(Call call) {
        this.log("onRemoteHeld");
    }

    @Override
    public void onRemoteUnheld(Call call) {
        this.log("onRemoteUnheld");
    }

    @Override
    public void onFrameSizeChanged(int i, int i1, VideoSurface.Endpoint endpoint, VideoSurface videoSurface) {
        log("onFrameSizeChanged");
    }

    @Override
    public void onSurfaceRenderingStarted(VideoSurface videoSurface) {
        log("onSurfaceRenderingStarted");
    }

    @Override
    public void onIncomingCall(Call call) {

        this.log("Received an inbound call from: " + call.getRemoteAddress());
        call.addListener(this);
        call.setVideoView(this.remoteSurface);
        call.answer(MediaDirection.SEND_AND_RECEIVE, MediaDirection.SEND_AND_RECEIVE);
    }

    @Override
    public void onCaptureSettingChange(PhoneVideoCaptureSetting phoneVideoCaptureSetting, int i) {
        this.log("onCaptureSettingChange");
    }

    @Override
    public void onLocalMediaStream() {
        this.log("onLocalMediaStream");
    }
}
