<?php

$username = $_GET['user'];

// configure the JSON to use in the session.
$json = '
    {
        "webAppId": "266b8233-d2db-4cbe-b6f2-505d284ddf0d",
        "allowedOrigins": ["*"],
        "urlSchemeDetails": {
            "host": "192.168.1.5",
            "port": "8080",
            "secure": false
        },
        "voice":
        {
            "username": "' . $username . '",
            "domain": "fusion.vbox",
            "inboundCallingEnabled": true,
			"allowedOutboundDestination": "all"
        }
    }
';

// configure the curl options
$ch = curl_init("http://fusion.vbox:8080/gateway/sessions/session");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
curl_setopt($ch, CURLOPT_HTTPHEADER, array(         
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json))
);

// execute HTTP POST & close the connection
$response = curl_exec($ch);
curl_close($ch);

// add CORS header - not necessary for Assist
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

// decode the JSON and pick out the ID
echo json_decode($response)->sessionid;